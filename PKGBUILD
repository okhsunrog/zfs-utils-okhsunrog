# Maintainer: okhsunrog <me@okhsunrog.ru>

pkgname=zfs-utils-okhsunrog
pkgver=2.1.10
pkgrel=1
pkgdesc="Userspace utilities for the Zettabyte File System."
arch=('x86_64')
url='https://zfsonlinux.org/'
license=('CDDL')
optdepends=('python: for arcstat/arc_summary/dbufstat')
provides=("${pkgname%-git}=${pkgver}")
conflicts=("${pkgname%-git}")
source=(
				"https://github.com/openzfs/zfs/releases/download/zfs-$pkgver/zfs-$pkgver.tar.gz"
        'zfs.initcpio.install'
        'zfs.initcpio.hook'
        'zfs.initcpio.zfsencryptssh.install')
sha256sums=('SKIP'
            'SKIP'
            'SKIP'
            'SKIP')
backup=('etc/default/zfs'
        'etc/zfs/zed.d/zed.rc')

prepare() {
    cd zfs-$pkgver

    # pyzfs is not built, but build system tries to check for python anyway
    ln -sf /bin/true python3-fake

    autoreconf -fi
}

build() {
    cd zfs-$pkgver

    ./configure \
        --prefix=/usr \
        --sysconfdir=/etc \
        --sbindir=/usr/bin \
        --with-mounthelperdir=/usr/bin \
        --with-udevdir=/usr/lib/udev \
        --libexecdir=/usr/lib/zfs \
        --enable-pyzfs=no \
        --enable-systemd \
        --with-config=user \
				--with-zfsexecdir=/usr/lib/zfs
    make
}

package() {
    cd zfs-$pkgver
    make DESTDIR="${pkgdir}" install

    # Remove uneeded files
    rm -r "${pkgdir}"/etc/init.d
    rm -r "${pkgdir}"/etc/sudoers.d #???
    # We're experimenting with dracut in [extra], so start installing this.
    #rm -r "${pkgdir}"/usr/lib/dracut
    rm -r "${pkgdir}"/usr/lib/modules-load.d
    rm -r "${pkgdir}"/usr/share/initramfs-tools
    rm -r "${pkgdir}"/usr/share/zfs/*.sh
    rm -r "${pkgdir}"/usr/share/zfs/{runfiles,test-runner,zfs-tests}
    # Install the support files
    install -D -m644 "${srcdir}"/zfs.initcpio.hook "${pkgdir}"/usr/lib/initcpio/hooks/zfs
    install -D -m644 "${srcdir}"/zfs.initcpio.install "${pkgdir}"/usr/lib/initcpio/install/zfs
    install -D -m644 "${srcdir}"/zfs.initcpio.zfsencryptssh.install "${pkgdir}"/usr/lib/initcpio/install/zfsencryptssh
    install -D -m644 contrib/bash_completion.d/zfs "${pkgdir}"/usr/share/bash-completion/completions/zfs
}
